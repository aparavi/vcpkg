#!/bin/bash

if [ $# -ne 3 ]
  then
    echo "usage ./create_archives.sh <plat> <compiler> <rev>"
	exit 1
fi

package() {
	local PKG=x64-$1-$2-aparavi-$3-$4.tgz
	echo "Creating $PKG"
	rm -f $PKG > /dev/null
	tar cfz $PKG $3
	if [[ "$OSTYPE" == "darwin"* ]]; then
		echo "$(shasum -a 512 $PKG) $PKG"
	else
		echo "$(sha512sum $PKG) $PKG"
	fi
}

package $1 $2 installed $3 &
package $1 $2 packages $3 &
package $1 $2 buildtrees $3 &

FAIL=0

for job in `jobs -p`
do
	wait $job || let "FAIL +1"
done

echo $FAIL
