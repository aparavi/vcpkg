set(VCPKG_TARGET_ARCHITECTURE x64)
set(VCPKG_CRT_LINKAGE dynamic)
set(VCPKG_LIBRARY_LINKAGE static)

set(VCPKG_CMAKE_SYSTEM_NAME Darwin)

# Common definitions across c++/c
set(TOOLCHAIN_DEFAULT_FLAGS "${TOOLCHAIN_DEFAULT_FLAGS} -Wno-trigraphs -Wno-unused-value -Wno-parentheses -Wno-unused-value")
set(TOOLCHAIN_DEFAULT_FLAGS "${TOOLCHAIN_DEFAULT_FLAGS} -Wno-parentheses -Wno-switch -fPIC -msse3 -mssse3 -msse4")
set(TOOLCHAIN_DEFAULT_FLAGS "${TOOLCHAIN_DEFAULT_FLAGS} -maes -msha")
set(CMAKE_CXX_FLAGS "${TOOLCHAIN_DEFAULT_FLAGS} -std=c++2a" CACHE STRING "" FORCE)
